
.PHONY: build install clean
.PHONY: docker-build
BINARY=tasker

build: $(BINARY)

$(BINARY):	
	go build -v

install:
	go install

clean:
	rm -rf $(BINARY)

docker-build:
	docker run -it -v ${GOPATH}:/go -w /go/src/gitlab.com/novirmor/tasker/ golang:1.11-stretch go build