package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sync"
)

type config struct {
	workDirName     string
	templateFile    string
	tasksDirName    string
	artifactDirName string
	logsDirName     string
	concurrency     int
}

func prepareDir(dir string) error {
	if _, err := os.Stat(dir); !os.IsNotExist(err) {
		os.RemoveAll(dir)
	}
	if err := os.Mkdir(dir, 0755); err != nil {
		return err
	}
	return nil
}

func (c config) doJob() error {
	taskDirs, err := ioutil.ReadDir(c.tasksDirName)
	if err != nil {
		return err
	}
	wg := sync.WaitGroup{}
	queue := make(chan *task, len(taskDirs))
	for _, taskDir := range taskDirs {
		if taskDir.IsDir() {
			queue <- newTask(taskDir.Name())
			wg.Add(1)
		}
	}
	go func() {
		wg.Wait()
		close(queue)
	}()
	for t := range queue {
		go func(t *task) {
			defer wg.Done()
			if err := c.doTask(t); err != nil {
				log.Println(err)
			}
			if err := c.collectArtifacts(t); err != nil {
				log.Println(err)
			}
		}(t)
	}
	return nil
}

func (c config) doTask(t *task) error {
	log.Printf("%s: Creating work tree\n", t.Name)
	if err := t.createWorkTree(c); err != nil {
		return fmt.Errorf("%s: %s", t.Name, err.Error())
	}
	log.Printf("%s: Parsing\n", t.Name)
	if err := t.parse(c); err != nil {
		return fmt.Errorf("%s: %s", t.Name, err.Error())
	}
	log.Printf("%s: Running\n", t.Name)
	if err := t.run(c); err != nil {
		return fmt.Errorf("%s: %s", t.Name, err.Error())
	}
	log.Printf("%s: Ended without error\n", t.Name)
	return nil
}

func (c config) collectArtifacts(t *task) error {
	log.Printf("Collecting outputfiles from: %s\n", t.Name)
	if err := t.collectOutput(c); err != nil {
		return fmt.Errorf("%s: %s", t.Name, err.Error())
	}
	log.Printf("Collecting log from: %s\n", t.Name)
	if err := t.collectLogs(c); err != nil {
		return fmt.Errorf("%s: %s", t.Name, err.Error())
	}
	return nil
}

func (c config) prepareDirs() error {
	if err := prepareDir(c.workDirName); err != nil {
		return err
	}
	if err := prepareDir(c.artifactDirName); err != nil {
		return err
	}
	if err := prepareDir(c.logsDirName); err != nil {
		return err
	}
	return nil
}

func (c config) clean() error {
	if _, err := os.Stat(c.workDirName); !os.IsNotExist(err) {
		err := os.RemoveAll(c.workDirName)
		if err != nil {
			return err
		}
	}
	return nil
}
