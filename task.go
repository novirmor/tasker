package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"text/template"
)

type task struct {
	Name      string
	Functions []string
	Variables map[string]string
}

func newTask(name string) *task {
	tmp := new(task)
	tmp.Name = name
	tmp.Variables = make(map[string]string)
	return tmp
}

func (t task) createWorkTree(c config) error {
	dirsToCreate := []string{"download", "output", "raw"}
	for _, d := range dirsToCreate {
		if err := os.MkdirAll(fmt.Sprintf("%s/%s/%s", c.workDirName, t.Name, d), 0755); err != nil {
			return err
		}
	}
	return nil
}

func (t *task) parse(c config) error {
	taskFile, err := os.Open(fmt.Sprintf("%s/%s/main.json", c.tasksDirName, t.Name))
	if err != nil {
		return err
	}
	defer taskFile.Close()
	tmp, err := ioutil.ReadAll(taskFile)
	if err != nil {
		return err
	}
	json.Unmarshal(tmp, &t)
	temp, err := template.ParseFiles(c.templateFile)
	if err != nil {
		return err
	}
	runFile, err := os.Create(fmt.Sprintf("%s/%s/run.sh", c.workDirName, t.Name))
	if err != nil {
		return err
	}
	err = temp.Execute(runFile, t)
	if err != nil {
		return err
	}
	return nil
}

func (t task) run(c config) error {
	logfile, err := os.Create(fmt.Sprintf("%s/%s/log.txt", c.workDirName, t.Name))
	if err != nil {
		return err
	}
	defer logfile.Close()
	cmd := exec.Command("sh", fmt.Sprintf("%s/%s/run.sh", c.workDirName, t.Name))
	cmd.Stdout = logfile
	cmd.Stderr = logfile
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

func (t task) collectOutput(c config) error {
	artifacts, err := ioutil.ReadDir(fmt.Sprintf("%s/%s/output", c.workDirName, t.Name))
	if err != nil {
		return err
	}
	for _, a := range artifacts {
		src, err := os.Open(fmt.Sprintf("%s/%s/output/%s", c.workDirName, t.Name, a.Name()))
		defer src.Close()
		if err != nil {
			return err
		}
		dst, err := os.Create(fmt.Sprintf("%s/%s", c.artifactDirName, a.Name()))
		defer dst.Close()
		if err != nil {
			return err
		}
		if _, err := io.Copy(dst, src); err != nil {
			return err
		}

	}
	return nil
}

func (t task) collectLogs(c config) error {
	logfile, err := os.Open(fmt.Sprintf("%s/%s/log.txt", c.workDirName, t.Name))
	defer logfile.Close()
	if err != nil {
		return err
	}
	dst, err := os.Create(fmt.Sprintf("%s/%s.txt", c.logsDirName, t.Name))
	defer dst.Close()
	if err != nil {
		return err
	}
	if _, err := io.Copy(dst, logfile); err != nil {
		return err
	}
	return nil
}
