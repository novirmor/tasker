package main

import "fmt"

func main() {
	app := config{"tmp", "templates/run.sh", "tasks", "output", "logs", 10}
	if err := app.prepareDirs(); err != nil {
		panic(err)
	}
	if err := app.doJob(); err != nil {
		panic(err)
	}
	if err := app.clean(); err != nil {
		panic(err)
	}

}

func test() {
	fmt.Println("hello")
}
