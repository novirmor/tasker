#!/usr/bin/env sh
# shellcheck disable=SC1083
# shellcheck disable=SC1054
# shellcheck disable=SC1056
# becouse it's a template
set -e
export WORK_DIR="./tmp/{{ .Name }}"
export DOWNLOAD_DIR="${WORK_DIR}/download"
export RAW_DIR="${WORK_DIR}/raw"
export LOGFILE="${WORK_DIR}/log.txt"
export OUTPUT_DIR="${WORK_DIR}/output"
{{ range $name, $value := .Variables }}
export {{ $name }}="{{ $value }}" 
{{ end }}
for f in ./tasks/{{ .Name }}/*.sh; do
	. $f
done


main(){
{{ range $f := .Functions }} 
{{ $f }}
{{ end }}
}
main